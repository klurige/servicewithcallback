package se.cpacsystems.detectorlibrary;
import se.cpacsystems.detectorlibrary.IMyServiceCallback;
interface IMyService {
    int add(int a, int b);
    void registerCallback(IMyServiceCallback callback);
}

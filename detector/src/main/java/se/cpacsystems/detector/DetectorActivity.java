package se.cpacsystems.detector;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DetectorActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(se.cpacsystems.detector.R.layout.activity_detector);
        findViewById(se.cpacsystems.detector.R.id.presenterbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("se.cpacsystems.presenter", "se.cpacsystems.presenter.MainActivity"));
                startActivity(intent);
            }
        });
    }

}

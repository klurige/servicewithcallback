package se.cpacsystems.detector;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.List;

import se.cpacsystems.detectorlibrary.IMyService;
import se.cpacsystems.detectorlibrary.IMyServiceCallback;

public class MyService extends Service {
    //private List<IMyServiceCallback> callbacks;
    private Thread worker;
    RemoteCallbackList<IMyServiceCallback> callbacks;

    public MyService() {
        callbacks = new RemoteCallbackList<>();
        worker = new Thread(new Runnable() {
            private int value;

            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    int n = callbacks.beginBroadcast();
                    try {
                        callbacks.getBroadcastItem(0).status(value);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    callbacks.finishBroadcast();
                    value++;
                }
            }
        });
        worker.start();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    private final IMyService.Stub binder = new IMyService.Stub() {

        @Override
        public int add(int a, int b) throws RemoteException {
            return a + b;
        }

        @Override
        public void registerCallback(IMyServiceCallback callback) throws RemoteException {
            callbacks.register(callback);
        }

    };
}

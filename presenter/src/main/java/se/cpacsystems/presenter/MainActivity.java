package se.cpacsystems.presenter;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.widget.TextView;

import se.cpacsystems.detectorlibrary.IMyService;
import se.cpacsystems.detectorlibrary.IMyServiceCallback;

public class MainActivity extends Activity implements IMyServiceCallback {
    private IMyService myService;
    private TextView status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        status = findViewById(R.id.status);

        findViewById(R.id.broadcastbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("se.cpacsystems.detector.ACTION_RUN");
                sendBroadcast(intent, "se.cpacsystems.detector.permission.DETECT");
            }
        });
        findViewById(R.id.bindbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serviceIntent = new Intent()
                        .setComponent(new ComponentName(
                                "se.cpacsystems.detector",
                                "se.cpacsystems.detector.MyService"));
                bindService(serviceIntent, connection, BIND_AUTO_CREATE);
            }
        });
        findViewById(R.id.unbindbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unbindService(connection);
            }
        });
        findViewById(R.id.addbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(myService != null) {
                            try {
                                final int result = myService.add(2,3);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        status.setText("Status: " + result);
                                    }
                                });
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
            }
        });
    }
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            myService = IMyService.Stub.asInterface(service);
            try {
                myService.registerCallback(MainActivity.this);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            myService = null;
        }
    };

    @Override
    public IBinder asBinder() {
        return null;
    }

    @Override
    public void status(final int value) throws RemoteException {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setText("Status: " + value);
            }
        });
    }
}
